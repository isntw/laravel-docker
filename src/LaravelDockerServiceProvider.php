<?php

namespace Isntw\LaravelDocker;

use Illuminate\Support\ServiceProvider;

class LaravelDockerServiceProvider extends ServiceProvider 
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->publishes([        
            __DIR__ . '/docker' => $this->app->basePath('docker'),
            __DIR__ . '/docker-compose.yml' => $this->app->basePath('docker-compose.yml'),
            __DIR__ . '/Dockerfile' => $this->app->basePath('Dockerfile'),
            __DIR__ . '/reverse-proxy' => $this->app->basePath('../reverse-proxy'),
        ], 'laravel-docker');

    }
}
