# README #
Laravel Docker Traefik reverse-proxy

### Steps to install ###

1. install repo
    ```bash
    composer require isntw/laravel-docker
    ```
2. publish repo
    ```bash
    php artisan vendor:publish --tag=laravel-docker
    ```
3. create docker network
    ```bash
    docker network create web
    ```
4. add/edit following lines in .env file
    ```env
    APP_NAME=example
    DB_CONNECTION=mysql
    DB_HOST=db-example
    DB_PORT=3306
    DB_DATABASE=example
    DB_USERNAME=root
    DB_PASSWORD=root
    ```